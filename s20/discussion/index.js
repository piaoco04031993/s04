// script
// let assets = [
// 	{id: 1,
// 	name: 'Hyundai'
// 	description: ModelK
// 	stock: 4
// 	isAvailable: 'Fasle'
// 	dateAdded: 'January 24,2022'

// 	{id: 2,
// 	name: 'Kia'
// 	description: "ModelP"
// 	stock: "0"
// 	inAvailable: "False"
// 	dateAdded: "January 22, 2022"}
// ]

// console.log(assets)

let assets = [
	{
		"id": "item,-1",
		"name": "Construction Crane",
		"description": "Doesn't actually fly. Weird",
		"stock": 5, 
		"isAvailable": true,
		"dateAdded": "December 29, 2022"
	},
	{
		"id": "item,-2",
		"name": "Backbone",
		"description": "It has a claw. Cool",
		"stock": 2,
		"isAvailable": true,
		"dateAdded": "January 24, 2022"
	}
];

console.log(assets);

// JSON

/*
	JSON -JavaScript object notation
		-is a string but formatted as JS objects
		- is popularly used to pass data from one  application to another
		-is not only i JS but also in Other prog. language to pass data

		-This is why it is specified as JavaScript Object Notation
		-There are JSON that are saved in a file with an extension called .json
		-There is a way to turn JSON as JS objects and there is a way to JS object to JSON

		JS Object are not the same as JSON
			- JSON is a string
			- JS Object is an object
			-JSON keys are surrounded with double syntax for Json

		{
			"key1": "valueA",
			"key2": "valueB"
		}

	--- Converting JS Objects into JSON ---
	 - this will allow us to convert/turn JS Object into JSON
	 - by first turning our JS Object into JSON, we can also turn JSON back into JS Objects
	 - This is commonly used when trying to pass data from application to another via the use HTTP request.
	 	- HTTP REquest are request for resource bw a server and a client (browser)

	 - JSON format is also used in database.
*/

// stringify

let batches = [
	{
		batchName: 'batch 156'
	},
	{
		batchName: 'batch 157'
	}
];

console.log(batches);

// convert
console.log(JSON.stringify(batches));